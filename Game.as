﻿package  {
	import flash.display.MovieClip;
	import TextPanel;
	
	//the main document class
	//remember that this is attached to the timeline via the properties panel
	//all game initialisation and creation of objects should happen in the Game constructor
	public class Game extends MovieClip{
		
		//this object created as a class member rather than as a local variable
		//so it can be accessed by other functions of the class
		public var item:DraggableItem;
		public var tp:TextPanel;

		public function Game() {
			// constructor code
			trace("constructor for game class")
			//make an item and add it to the screen
			item=new DraggableItem();
			item.x=200;
			item.y=200;
			addChild(item);
			
			//make a textpanel and add to the screen
			tp = new TextPanel();
			addChild(tp);
			tp.SetPos(100,100);
			tp.SetSize(300,50);
			 //so that item knows about textpanel
			item.tp = tp;
			

		}

	}
	
}
