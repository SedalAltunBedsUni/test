﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	//class implements simple drag and drop
	//for demonstration purposes it also communicates with the TextPanel singleton
	
	public class DraggableItem extends MovieClip{


		public var tp:TextPanel;

		public function DraggableItem() {
			// constructor code
			trace ("constructor for draggable item");
			this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownListener);
			this.addEventListener(MouseEvent.MOUSE_UP, mouseUpListener);
		}
		
		
		public function mouseDownListener(e:Event)
		{
		this.startDrag();
		tp.SetText("oi, get off me");
		}
		
		public function mouseUpListener(e:Event)
		{
		this.stopDrag();
		tp.SetText("ah, that's better");
		}

	}
	
}
