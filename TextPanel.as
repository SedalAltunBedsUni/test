﻿package  {
	
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	

	public class TextPanel extends MovieClip {
		
		//a textField, but you could add other ones too
		public var textField:TextField ;
		//textFormat used to control how text looks
		public var textFormat:TextFormat;

		
		public function TextPanel() {
			// constructor code
			textField = new TextField();
			addChild(textField);
			textField.x=0;
			textField.y=0;
			textField.text = "text goes here";
			
			//make it resize if text is too large
			textField.autoSize=TextFieldAutoSize.CENTER;
			textField.text="text will go here";

			//create a new textformat object
			//this is used to choose font, colour, size 
			//for the textfield
			textFormat = new TextFormat();
			textFormat.size=32;
			textFormat.font = "Arial";
			textField.defaultTextFormat=textFormat;
			textField.setTextFormat(textFormat);
		}
		
		public function SetText(txt:String)
		{
			textField.text =txt;
			
		}
		
		public function SetSize(wid:int, hei:int)
		{
			textField.width = wid;
			textField.height = hei;
		}
		
		public function SetPos(xpos:int, ypos:int)
		{
			textField.x=xpos;
			textField.y=ypos;
		}
		


	}
	
}
